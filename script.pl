use strict;
use warnings;
use 5.010;
 
my $path = shift || '.';
print("Type the name of the output file. Ex.: metrics.csv\n");
my $nameOutput = <STDIN>;

traverse($path);
 
sub traverse {
    my ($pathName) = @_;

  	if($pathName =~ /\.py$/i) {
    	system("pymetrics $pathName -c $nameOutput");
	}

    return if not -d $pathName;
    opendir my $dh, $pathName or die;
    while (my $sub = readdir $dh) {
        next if $sub eq '.' or $sub eq '..';
        traverse("$pathName/$sub");
    }
    close $dh;
    return;
}